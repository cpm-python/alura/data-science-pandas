#!/usr/bin/env python
# coding: utf-8

# # Relatório de anális 6

# ## Criando novas variáveis

# In[1]:


import pandas as pd


# In[4]:


dados = pd.read_csv('dados/aluguel_residencial.csv', sep=';')
dados.head(12)


# In[7]:


dados['Valor Bruto'] = dados['Valor'] + dados['Condominio'] + dados['IPTU']


# In[8]:


dados.head(12)


# In[10]:


dados['Valor m2'] = dados['Valor'] / dados['Area']
dados['Valor m2'] = dados['Valor m2'].round(2)
dados.head(12)


# In[11]:


dados['Valor Bruto m2'] = (dados['Valor Bruto'] / dados['Area']).round(2)
dados.head(12)


# In[16]:


casas = ['Casa', 'Casa de Condomínio', 'Casa de Vila']
casas


# In[17]:


dados['Tipo Agregado'] = dados['Tipo'].apply(lambda x: 'Casa' if x in casas else 'Apartamento')
dados.head(12)


# ## Excluindo variáveis

# In[19]:


dados_aux = pd.DataFrame(dados[['Tipo Agregado', 'Valor m2', 'Valor Bruto', 'Valor Bruto m2']])
dados_aux.head(12)


# In[20]:


del dados_aux['Valor Bruto']


# In[21]:


dados_aux.head(12)


# In[22]:


dados_aux.pop('Valor Bruto m2')


# In[23]:


dados_aux.head(12)


# In[24]:


dados.drop(['Valor Bruto', 'Valor Bruto m2'], axis = 1, inplace = True)
dados.head(12)


# In[25]:


dados.to_csv('dados/aluguel_residencial.csv', sep=';', index = False)


# In[ ]:




