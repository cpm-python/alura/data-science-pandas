#!/usr/bin/env python
# coding: utf-8

# # Relatório de Analise 1

# ## Imporanto a base de dados

# In[1]:


import pandas as pd


# In[2]:


# Imporanto
pd.read_csv('dados/aluguel.csv', sep=';')


# In[3]:


dados = pd.read_csv('dados/aluguel.csv', sep=';')

dados


# In[4]:


type(dados)


# 

# In[5]:


dados.info()


# In[6]:


dados.head(10)


# ## Informações gerais sobre a base de dados

# In[7]:


dados.dtypes


# In[11]:


tipos_de_dados = pd.DataFrame(dados.dtypes, columns = ['Tipos de dados'])
tipos_de_dados.columns.name = 'Variáveis'
tipos_de_dados


# In[17]:


print(f'A base de dados apresenta {dados.shape[0]} registos (imóveis) e {dados.shape[1]} variáveis')

