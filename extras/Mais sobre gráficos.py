#!/usr/bin/env python
# coding: utf-8

# # Mais sobre gráficos

# In[1]:


import pandas as pd


# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
plt.rc('figure', figsize = (15, 8))


# In[3]:


dados = pd.read_csv("dados/aluguel.csv", sep =";")
dados.head(2)


# In[4]:


area = plt.figure()


# In[5]:


g1 = area.add_subplot(2, 2, 1)
g2 = area.add_subplot(2, 2, 2)
g3 = area.add_subplot(2, 2, 3)
g4 = area.add_subplot(2, 2, 4)


# In[6]:


g1.scatter(dados.Valor, dados.Area)
g1.set_title('Valor x Área')

g2.hist(dados.Valor)
g2.set_title('Histograma')

dados_g3 = dados.Valor.sample(100)
dados_g3.index = range(dados_g3.shape[0])
g3.plot(dados_g3)
g3.set_title('Plot exemplo')

grupo = dados.groupby('Tipo')['Valor']
lavel = grupo.mean().index
valores = grupo.mean().values
g4.bar(lavel, valores)
g4.set_title('Valor médio por tipo')

area


# In[7]:


area.savefig('dados/grafico.png', dpi = 300, bbox_inches = 'tight')


# In[ ]:




