#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


dados = pd.read_csv('dados/aluguel.csv', sep = ';')
dados.head(12)


# In[4]:


classes = [0, 2, 4, 6, 99]
quartos = pd.cut(dados.Quartos, classes)
quartos


# In[5]:


pd.value_counts(quartos)


# In[10]:


labels = ['1 e 2 quartos', '3 e 4  quartos', '5 e 6 quartos', 'Mais de 6 quartos']
labels


# In[11]:


quartos = pd.cut(dados.Quartos, classes, labels = labels)
pd.value_counts(quartos)


# In[13]:


quartos = pd.cut(dados.Quartos, classes, labels = labels, include_lowest = True)
pd.value_counts(quartos)


# In[ ]:




