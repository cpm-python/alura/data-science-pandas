#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


json = open('dados/aluguel.json')
print(json.read())


# In[3]:


df_json = pd.read_json('dados/aluguel.json')
df_json


# In[4]:


txt = open('dados/aluguel.txt')
print(txt.read())


# In[5]:


df_txt = pd.read_table('dados/aluguel.txt', sep='\t')
df_txt


# In[8]:


df_html = pd.read_html('https://economia.uol.com.br/cotacoes/')


# In[13]:


df_html[3]

