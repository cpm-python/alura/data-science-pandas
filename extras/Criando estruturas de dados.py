#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# # Series

# In[2]:


data = [1, 2, 3, 4, 5]


# In[3]:


s = pd.Series(data)
s


# In[4]:


index = ['linha ' + str(i) for i in range(5) ]


# In[5]:


index


# In[6]:


s = pd.Series(data=data, index= index)
s


# In[7]:


data = {'linha ' + str(i) : i + 1 for i in range(5)}
data


# In[8]:


s = pd.Series(data)
s


# In[9]:


s1 = s + 2
s1


# In[10]:


s2 = s + s1
s2


# ## DataFrame

# In[11]:


data = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
data


# In[ ]:





# In[12]:


index = ['Linha ' + str(i) for i in range(3)]
index


# In[13]:


df1 = pd.DataFrame(data = data, index = index)
df1


# In[14]:


columns = ['Coluna ' + str(i) for i in range(3)]
columns


# In[15]:


df1 = pd.DataFrame(data = data, index = index, columns = columns)
df1


# In[16]:


data = {'Coluna 0': {'Linha 0': 1, 'Linha 1': 4, 'Linha 2': 7},
        'Coluna 1': {'Linha 0': 2, 'Linha 1': 5, 'Linha 2': 8},
        'Coluna 2': {'Linha 0': 3, 'Linha 1': 6, 'Linha 2': 9}}
data


# In[17]:


df2 = pd.DataFrame(data)
df2


# In[18]:


data = [(1, 2, 3), (4, 5, 6), (7, 8, 9)]
df3 = pd.DataFrame(data = data, index = index, columns = columns)
df3


# In[19]:


df1[df1 > 0] = 'A'
df2[df2 > 0] = 'B'
df3[df3 > 0] = 'C'


# In[20]:


df4 = pd.concat([df1, df2, df3])


# In[22]:


df4


# In[24]:


df4 = pd.concat([df1, df2, df3], axis = 1)
df4


# In[ ]:




