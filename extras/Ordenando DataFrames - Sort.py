#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd


# In[14]:


data = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
data


# In[15]:


list('321')


# In[16]:


df = pd.DataFrame(data, list('321'), list('zyx'))
df


# In[17]:


df.sort_index(inplace = True)
df


# In[18]:


df.sort_index(inplace = True, axis=1)
df


# In[19]:


df.sort_values(by = 'x', inplace = True)
df


# In[21]:


df.sort_values(by = '3', axis = 1, inplace = True)
df


# In[ ]:




