#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[3]:


data = [0.5, None, None, 0.52, 0.54, None, None, 0.59, 0.6, None, 0.7]
s = pd.Series(data)
s


# In[4]:


s.fillna(0)


# In[5]:


s


# In[6]:


s.fillna(method = 'ffill')


# In[7]:


s.fillna(method = 'bfill')


# In[9]:


s.fillna(s.mean())


# In[11]:


s.fillna(method = 'ffill', limit = 1)


# In[12]:


s1 = s.fillna(method = 'ffill', limit = 1)


# In[13]:


s1.fillna(method = 'bfill', limit = 1)


# In[ ]:




