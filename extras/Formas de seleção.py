#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd


# In[3]:


data = [(1, 2, 3, 4),
        (5, 6, 7, 8),
        (8, 10, 11, 12),
        (13, 14, 15, 16)]
df = pd.DataFrame(data, 'l1 l2 l3 l4'.split(), 'c1 c2 c3 c4'.split())
df


# In[5]:


'l1 l2 l3 l4'.split() 


# In[7]:


df['c1']


# In[8]:


type(df['c1'])


# In[10]:


df[['c3', 'c1']]


# In[11]:


type(df[['c3', 'c1']])


# In[17]:


df[1:3]


# In[18]:


df[1:3][['c3', 'c1']]


# In[21]:


df.loc[['l3', 'l2']]


# In[23]:


df.loc['l1', 'c2']


# In[26]:


df.iloc[0, 1]


# In[28]:


df.loc[['l3','l1'], ['c4', 'c1']]


# In[30]:


df.iloc[[2,0], [3, 0]]

