#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
alunos = pd.DataFrame({'Nome': ['Ary', 'Cátia', 'Denis', 'Beto', 'Bruna', 'Dara', 'Carlos', 'Alice'], 
                        'Sexo': ['M', 'F', 'M', 'M', 'F', 'F', 'M', 'F'], 
                        'Idade': [15, 27, 56, 32, 42, 21, 19, 35], 
                        'Notas': [7.5, 2.5, 5.0, 10, 8.2, 7, 6, 5.6], 
                        'Aprovado': [True, False, False, True, True, True, False, False]}, 
                        columns = ['Nome', 'Idade', 'Sexo', 'Notas', 'Aprovado'])


# In[2]:


alunos.head(20)


# In[4]:


sexo = alunos.groupby('Sexo')
sexo


# In[5]:


sexo = pd.DataFrame(sexo['Notas'].mean().round(2))
sexo


# In[6]:


sexo.columns = ['Notas Médias']
sexo


# In[ ]:




