#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


dados = pd.read_csv('dados/aluguel_amostra.csv', sep = ';')
dados.head(2)


# In[9]:


get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
plt.rc('figure', figsize = (14, 6))


# In[10]:


dados.boxplot(['Valor'])


# In[3]:


valor = dados['Valor m2']
valor


# In[4]:


q1 = valor.quantile(.25)
q1


# In[5]:


q3 = valor.quantile(.75)
q3


# In[6]:


iiq = q3 - q1
iiq


# In[7]:


limite_inferior = q1 - 1.5 * iiq
limite_inferior


# In[11]:


limite_superior = q3 + 1.5 * iiq
limite_superior


# In[13]:


selecao = (valor >= limite_inferior) & (valor <= limite_superior)
dados_new = dados[selecao]
dados_new.head(12)


# In[14]:


dados_new.boxplot(['Valor'])


# In[15]:


dados.hist(['Valor'])


# In[16]:


dados_new.hist(['Valor'])


# In[ ]:




