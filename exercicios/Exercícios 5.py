#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


alunos = pd.DataFrame({'Nome': ['Ary', 'Cátia', 'Denis', 'Beto', 'Bruna', 'Dara', 'Carlos', 'Alice'], 
                        'Sexo': ['M', 'F', 'M', 'M', 'F', 'F', 'M', 'F'], 
                        'Idade': [15, 27, 56, 32, 42, 21, 19, 35], 
                        'Notas': [7.5, 2.5, 5.0, 10, 8.2, 7, 6, 5.6], 
                        'Aprovado': [True, False, False, True, True, True, False, False]}, 
                        columns = ['Nome', 'Idade', 'Sexo', 'Notas', 'Aprovado'])
alunos


# In[3]:


selecao = alunos.Aprovado == True
aprovados = alunos[selecao]
aprovados


# In[4]:


selecao = (alunos.Aprovado == True) & (alunos.Sexo == 'F')
aprovadas = alunos[selecao]
aprovadas


# In[6]:


selecao = (alunos.Idade > 10) & (alunos.Idade < 20) | (alunos.Idade >= 40)
alunos[selecao]


# In[8]:


selecao = alunos['Aprovado'] == False
reprovados = alunos[['Nome', 'Sexo', 'Idade']][selecao]
reprovados


# In[10]:


selecao = alunos['Aprovado'] == False
reprovados = alunos.loc[selecao, ['Nome', 'Sexo', 'Idade']]
reprovados


# In[12]:


alunos.sort_values(by = ['Idade'], inplace = True)
alunos.iloc[:3]

