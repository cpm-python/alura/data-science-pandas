#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


alunos = pd.DataFrame({'Nome': ['Ary', 'Cátia', 'Denis', 'Beto', 'Bruna', 'Dara', 'Carlos', 'Alice'], 
                        'Sexo': ['M', 'F', 'M', 'M', 'F', 'F', 'M', 'F'], 
                        'Idade': [15, 27, 56, 32, 42, 21, 19, 35], 
                        'Notas': [7.5, 2.5, 5.0, 10, 8.2, 7, 6, 5.6]}, 
                        columns = ['Nome', 'Idade', 'Sexo', 'Notas'])
alunos


# In[5]:


alunos['Notas-Média(Notas)'] = alunos['Notas'].apply(lambda x: x - alunos['Notas'].mean())
    


# In[8]:


alunos['Faixa Etária'] = alunos['Idade'].apply(lambda x: 'Menor que 20 anos' if x < 20 else ('Entre 20 e 40 anos' if (x >= 20 and x <= 40) else 'Maior que 40 anos'))


# In[10]:


alunos['Faixa Etária'] = alunos['Idade'].apply(lambda x: 'Menor que 20 anos' if x < 20 elif ('Entre 20 e 40 anos' if (x >= 20 and x <= 40) else 'Maior que 40 anos'))


# In[12]:


alunos['Notas-Média(Notas)'] = alunos.Notas - alunos.Notas.mean()


# In[13]:


alunos


# In[ ]:




