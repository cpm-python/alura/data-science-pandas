#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


atletas = pd.DataFrame([['Marcos', 9.62], ['Pedro', None], ['João', 9.69], 
                        ['Beto', 9.72], ['Sandro', None], ['Denis', 9.69], 
                        ['Ary', None], ['Carlos', 9.74]], 
                        columns = ['Corredor', 'Melhor Tempo'])
atletas


# In[3]:


atletas.fillna(atletas.mean(), inplace = True)


# In[4]:


atletas


# In[ ]:




