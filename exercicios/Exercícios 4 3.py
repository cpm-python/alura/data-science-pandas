#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd


# In[5]:


numeros = [i for i in range(11)]
letras = [chr(i + 65) for i in range(11)]
nome_coluna = ['N']

df = pd.DataFrame(data = numeros, index = letras, columns = nome_coluna)
df


# In[7]:


selecao = df['N'].isin([i for i in range(11) if i % 2 == 0])
df = df[selecao]
df


# In[ ]:




