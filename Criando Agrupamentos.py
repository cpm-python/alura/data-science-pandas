#!/usr/bin/env python
# coding: utf-8

# # Relatório de Análise 6

# ## Criando agrupamentos

# In[1]:


import pandas as pd


# In[2]:


dados = pd.read_csv('dados/aluguel_residencial.csv', sep=";")
dados.head(12)


# In[3]:


dados['Valor'].mean()


# In[4]:


dados['Valor'].mode()


# In[5]:


# dados.Bairro.unique()


# In[6]:


# bairros = dados.Bairro.unique()
bairros = ['Barra da Tijuca', 'Botafogo', 'Copacabana', 'Flamengo', 'Ipanema', 'Leblon', 'Tijuca']
bairros


# In[7]:


selecao = dados['Bairro'].isin(bairros)
selecao


# In[8]:


dados = dados[selecao]


# In[9]:


dados


# In[10]:


dados['Bairro'].drop_duplicates()


# In[11]:


grupo_bairro = dados.groupby('Bairro')
grupo_bairro


# In[12]:


type(grupo_bairro)


# In[13]:


grupo_bairro.groups


# In[14]:


for bairro, data in grupo_bairro:
    print(f'{bairro} -> {data.Valor.mean()}')


# In[15]:


grupo_bairro['Valor'].mean()


# In[16]:


grupo_bairro[['Valor', 'Condominio']].mean().round(2)


# ## Estatiticas descritivas

# In[17]:


grupo_bairro['Valor'].describe().round(2)


# In[18]:


grupo_bairro['Valor'].aggregate(['min', 'max', 'sum']).round(2).rename(columns = {'min': 'Mínimo', 'max' : 'Máximo', 'sum': 'Soma'})


# In[19]:


get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
plt.rc('figure', figsize=(20,10))


# In[20]:


fig = grupo_bairro.Valor.std().plot.bar(color= 'blue')
fig.set_ylabel('Valor do Aluguel')
fig.set_title('Valor médio de aluguel por bairro', {'fontsize': 22})
fig


# In[61]:





# In[ ]:




