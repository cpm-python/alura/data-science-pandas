#!/usr/bin/env python
# coding: utf-8

# # Relatorio de análise 3

# ## Imóveis residenciais

# In[1]:


import pandas as pd


# In[2]:


dados = pd.read_csv('dados/aluguel.csv', sep = ';')
dados.head(10)


# In[3]:


list(dados['Tipo'].drop_duplicates())


# In[4]:


residencial = ['Quitinete',
 'Casa',
 'Apartamento',
 'Casa de Condomínio',
 'Casa de Vila']
residencial


# In[5]:


dados.head()


# In[6]:


selecao = dados['Tipo'].isin(residencial)
selecao.head()


# In[7]:


dados_residencial = dados[selecao]
dados_residencial


# In[8]:


list(dados_residencial['Tipo'].drop_duplicates())


# In[9]:


dados_residencial.shape[0]


# In[10]:


dados.shape[0]


# In[11]:


dados_residencial.index = range(len(dados_residencial))


# In[12]:


dados_residencial.head()


# ## Exportanto a base de dados

# In[13]:


dados_residencial.to_csv('dados/aluguel_residencial.csv', sep = ';')


# In[15]:


dados_residencial_2 = pd.read_csv('dados/aluguel_residencial.csv', sep =';')
dados_residencial_2.head()


# In[16]:


dados_residencial.to_csv('dados/aluguel_residencial.csv', sep = ';', index = False)
dados_residencial_2 = pd.read_csv('dados/aluguel_residencial.csv', sep =';')
dados_residencial_2.head()


# In[ ]:




