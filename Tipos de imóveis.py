#!/usr/bin/env python
# coding: utf-8

# # Relatório de Análise 2

# ## Tipo de imóveis

# In[2]:


import pandas as pd


# In[4]:


dados = pd.read_csv('dados/aluguel.csv', sep = ';')
dados.head(10)


# In[6]:


dados['Tipo']


# In[9]:


tipo_de_imovel = dados['Tipo']
type(tipo_de_imovel)


# In[12]:


tipo_de_imovel.drop_duplicates()


# In[15]:


tipo_de_imovel.drop_duplicates(inplace=True)


# ## Organizando a visialização

# In[17]:


tipo_de_imovel = pd.DataFrame(tipo_de_imovel)
tipo_de_imovel


# In[19]:


tipo_de_imovel.index


# In[21]:


range(tipo_de_imovel.shape[0])


# In[23]:


for i in range(tipo_de_imovel.shape[0]):
    print(i)


# In[24]:


tipo_de_imovel.index = range(tipo_de_imovel.shape[0])


# In[25]:


tipo_de_imovel.shape[0]


# In[26]:


tipo_de_imovel


# In[29]:


tipo_de_imovel.columns.name = 'Id'
tipo_de_imovel


# In[ ]:




