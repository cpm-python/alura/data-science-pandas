#!/usr/bin/env python
# coding: utf-8

# # Relatório de anális 5

# ## Tratamento de dados faltantes

# In[1]:


import pandas as pd


# In[2]:


dados = pd.read_csv('dados/aluguel_residencial.csv', sep = ';')
dados.head(10)


# In[3]:


dados.isnull()


# In[4]:


dados.notnull()


# In[5]:


dados.info()


# In[6]:


dados[dados['Valor'].isnull()]


# In[7]:


a = dados.shape[0]
dados.dropna(subset = ['Valor'], inplace = True)
b = dados.shape[0]
a-b


# In[8]:


dados[dados['Valor'].isnull()]


# ## Continuação

# In[9]:


dados[dados['Condominio'].isnull()].shape[0]


# In[10]:


selecao = (dados['Tipo'] == 'Apartamento') & (dados['Condominio'].isnull())


# In[11]:


a = dados.shape[0]
dados = dados[~selecao]
b = dados.shape[0]
a-b


# In[13]:


dados[dados['Condominio'].isnull()].shape[0]


# In[16]:


dados = dados.fillna({'Condominio':0, 'IPTU': 0})
dados


# In[18]:


dados[dados['Condominio'].isnull()].shape[0]


# In[19]:


dados[dados['IPTU'].isnull()].shape[0]


# In[20]:


dados.info()


# In[21]:


dados.to_csv('dados/aluguel_residencial.csv', sep = ';', index= False)


# In[ ]:




