#!/usr/bin/env python
# coding: utf-8

# # Relatório de Anális 4

# ## Seleções e frequências

# In[1]:


import pandas as pd


# In[2]:


dados = pd.read_csv('dados/aluguel_residencial.csv', sep = ';')
dados.head()


# In[7]:


# Selecione somente os imóveis classificados com tipo 'Apartamento'.
selecao = dados['Tipo'] == 'Apartamento'
n1 = dados[selecao].shape[0]
n1


# In[12]:


# Selecione os imóveis classificados com tipos 'Casa', 'Casa de Condomínio' e 'Casa de Vila'.
selecao = (dados['Tipo'] == 'Casa') | (dados['Tipo'] == 'Casa de Condomínio') | (dados['Tipo'] == 'Casa de Vila')
n2 = dados[selecao].shape[0]
n2


# In[17]:


# Selecione os imóveis com área entre 60 e 100 metros quadrados, incluindo os limites.
# 60 >= area <= 100
selecao = (dados['Area'] >= 60) & (dados['Area'] <= 100)
n3 = dados[selecao].shape[0]
n3


# In[22]:


# Selecione os imóveis que tenham pelo menos 4 quartos e aluguel menor que R$ 2.000,00.
selecao = (dados['Quartos'] >= 4) & (dados['Valor'] < 2000)
n4 = dados[selecao].shape[0]
n4


# In[24]:


print(f'Número de imóveis classificados com tipo \'Apartamento\'. -> {n1}')
print(f'Número de imóveis classificados com tipos \'Casa\', \'Casa de Condomínio\' e \'Casa de Vila\'. -> {n2}')
print(f'Número de imóveis com área entre 60 e 100 metros quadrados, incluindo os limites. -> {n3}')
print(f'Número de imóveis que tenham pelo menos 4 quartos e aluguel menor que R$ 2.000,00. -> {n4}')

