#!/usr/bin/env python
# coding: utf-8

# # Relatório de Análise 8

# ## Identificando e Removendo Outliers

# In[1]:


import pandas as pd


# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
plt.rc('figure', figsize = (14, 6))


# In[3]:


dados = pd.read_csv('dados/aluguel_residencial.csv', sep = ';')
dados.head(12)


# In[4]:


dados.boxplot(['Valor'])


# In[5]:


dados[dados.Valor >= 500000]


# In[6]:


valor = dados.Valor
valor


# In[7]:


q1 = valor.quantile(.25)
q1


# In[8]:


q3 = valor.quantile(.75)
q3


# In[9]:


iiq = q3 - q1
iiq


# In[10]:


limite_inferior = q1 - 1.5 * iiq
limite_inferior


# In[11]:


limite_superior = q3 + 1.5 * iiq
limite_superior


# In[12]:


selecao = (valor >= limite_inferior) & (valor <= limite_superior)


# In[13]:


dados_new = dados[selecao]


# In[14]:


dados_new.head(12)


# In[15]:


dados_new[dados_new.Valor >= 500000]


# In[16]:


dados_new.boxplot(['Valor'])


# In[17]:


dados.hist(['Valor'])


# In[18]:


dados_new.hist(['Valor'])


# ## Continuação

# In[19]:


dados.boxplot(['Valor'], by = ['Tipo'])


# In[20]:


dados_new.boxplot(['Valor'], by = ['Tipo'])


# In[21]:


grupo_tipo = dados.groupby('Tipo')['Valor']
type(grupo_tipo)


# In[22]:


q1 = grupo_tipo.quantile(.25)
q1


# In[23]:


q3 = grupo_tipo.quantile(.75)
q3


# In[24]:


iiq = q3 - q1
iiq


# In[25]:


limite_inferior = q1 - 1.5 * iiq
limite_inferior


# In[26]:


limite_superior = q3 + 1.5 * iiq
limite_superior


# In[27]:


limite_superior.Apartamento


# <img src='dados/compressed_box-plot.png' width='70%'>

# In[32]:


dados_new = pd.DataFrame()
for tipo in grupo_tipo.groups.keys():
    eh_tipo = dados.Tipo == tipo
    eh_dentro_limite = (dados.Valor >= limite_inferior[tipo]) & (dados.Valor <= limite_superior[tipo])
    selecao = eh_tipo & eh_dentro_limite
    dados_selecao = dados[selecao]
    dados_new = pd.concat([dados_new, dados_selecao])


# In[35]:


dados_new.head(10)


# In[36]:


dados_new.boxplot(['Valor'], by = ['Tipo'])


# In[38]:


dados_new.to_csv('dados/aluguel_residencial_sem_outliers.csv', sep =";", index = False)


# In[39]:


dados_new.hist(['Valor'])

